## Recruitment management system

A Recruitment system manager is a script built for the HR and Consulting companies. Script manage the job posting for multiple companies. More than one candidate applies for a job after registration. Job application and Interview can be managed also with different stages and statuses. You can create an Interview and schedule it for companies.

# Laravel + Vue.js with Ant Design

![Example Image](https://gitlab.com/aldo_k/e-rekrutment/-/raw/main/demo/demo1.png)
![Example Image](https://gitlab.com/aldo_k/e-rekrutment/-/raw/main/demo/demo2.png)